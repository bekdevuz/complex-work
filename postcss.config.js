const purgecss = require('@fullhuman/postcss-purgecss');

module.exports = {
  plugins: [
    purgecss({
      content: ['./dist/css/*.css'],
    }),
    require('autoprefixer'),
    require('cssnano')({
      preset: 'default',
    }),
  ],
};
