const burgerMenu = document.querySelector('.burger-menu');
const headerMenu = document.querySelector('.header__menu');
const logo = document.querySelector('.hero__logo');

burgerMenu.addEventListener('click', () => {
  logo.classList.toggle('hidden');
  burgerMenu.classList.toggle('open');
  headerMenu.classList.toggle('open');
});

const menuItems = document.querySelectorAll('.header__menu li a');
menuItems.forEach((menuItem) => {
  menuItem.addEventListener('click', () => {
    logo.classList.remove('hidden');
    burgerMenu.classList.remove('open');
    headerMenu.classList.remove('open');
  });
});
